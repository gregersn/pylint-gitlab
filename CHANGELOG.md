# Changelog

## Unreleased

### Changed

* Change license from MIT to GPLv3

### Removed

* Drop support for Python 3.7

## 1.2.0

### Changed

* Better adhere message metadata to codeclimate schema [!12](https://gitlab.com/smueller18/pylint-gitlab/-/merge_requests/12)

### Fixed

* Fix up missing output on pylint>=2.15 [#17](https://gitlab.com/smueller18/pylint-gitlab/-/issues/17), [!13](https://gitlab.com/smueller18/pylint-gitlab/-/merge_requests/13)
* Updated dependencies to latest versions [#11](https://gitlab.com/smueller18/pylint-gitlab/-/issues/11), [#16](https://gitlab.com/smueller18/pylint-gitlab/-/issues/16)

## 1.1.0

### Added

* Add new reporter `GitlabCodeClimateReporterNoHash` [!11](https://gitlab.com/smueller18/pylint-gitlab/-/merge_requests/11)

## 1.0.0

### Added

* Add support for Python 3.10 [!9](https://gitlab.com/smueller18/pylint-gitlab/-/merge_requests/9)

### Changed

* Change fingerprint algorithm from md5 to sha256 [#10](https://gitlab.com/smueller18/pylint-gitlab/-/issues/10)
* Pipenv update [!9](https://gitlab.com/smueller18/pylint-gitlab/-/merge_requests/9)

### Removed

* Drop support for Python 3.6 [!9](https://gitlab.com/smueller18/pylint-gitlab/-/merge_requests/9)
